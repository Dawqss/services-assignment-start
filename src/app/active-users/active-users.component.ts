import { Component, Input} from '@angular/core';
import { ChangeStatusService } from "../change-status.service";
import {CounterChangeService} from "../counter-change.service";

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent {
  @Input() users: string[];

  constructor(private changeStatusService: ChangeStatusService, private counterChange: CounterChangeService) {}

  onSetToInactive(id: number) {
    this.changeStatusService.onSetToInactive(id);
    this.counterChange.changeStatus.emit(false);
  }
}
