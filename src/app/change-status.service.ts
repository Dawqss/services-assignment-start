import { Injectable } from '@angular/core';
import {CounterChangeService} from "./counter-change.service";

@Injectable()
export class ChangeStatusService {
  activeUsers = ['Max', 'Anna'];
  inactiveUsers = ['Chris', 'Manu'];

  constructor(private counterChange: CounterChangeService) { }

  onSetToInactive(id: number) {
      this.inactiveUsers.push(this.activeUsers[id]);
      this.activeUsers.splice(id, 1);

  }

  onSetToActive(id: number) {
      this.activeUsers.push(this.inactiveUsers[id]);
      this.inactiveUsers.splice(id, 1);
  }
}
