import {Component, OnInit} from '@angular/core';
import {ChangeStatusService} from "./change-status.service";
import {CounterChangeService} from "./counter-change.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  activeUsers = [];
  inactiveUsers = [];
  activeCount = 0;
  inactiveCount = 0;




  constructor(private changeStatusService: ChangeStatusService, private counterChange: CounterChangeService) {
    this.counterChange.changeStatus.subscribe(
        (status: boolean) => {
          if (status === false) {
            this.activeCount++;
          } else {
            this.inactiveCount++;
          }
        }
    )
  }

  ngOnInit() {
    this.activeUsers = this.changeStatusService.activeUsers;
    this.inactiveUsers = this.changeStatusService.inactiveUsers;
  }
}
