import { Component, Input,} from '@angular/core';
import {ChangeStatusService} from "../change-status.service";
import {CounterChangeService} from "../counter-change.service";

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent {
  @Input() users: string[];

  constructor(private changeStatusService: ChangeStatusService, private counterChange: CounterChangeService) {

  }

  onSetToActive(id: number) {
    this.changeStatusService.onSetToActive(id);
    this.counterChange.changeStatus.emit(true);
  }
}
