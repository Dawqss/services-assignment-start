import {EventEmitter} from "@angular/core";

export class CounterChangeService {
  changeStatus = new EventEmitter<Boolean>();
}
